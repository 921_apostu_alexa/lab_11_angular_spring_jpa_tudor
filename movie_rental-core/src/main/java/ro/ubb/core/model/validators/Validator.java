package ro.ubb.core.model.validators;

import ro.ubb.core.model.exceptions.ValidatorException;

public interface Validator<T> {
    void validate(T entity) throws ValidatorException;
}
