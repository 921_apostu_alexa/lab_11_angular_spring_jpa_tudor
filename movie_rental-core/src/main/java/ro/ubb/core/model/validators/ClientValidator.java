package ro.ubb.core.model.validators;

import ro.ubb.core.model.Client;
import ro.ubb.core.model.exceptions.BaseException;
import ro.ubb.core.model.exceptions.ValidatorException;

import java.util.Optional;

public class ClientValidator implements Validator<Client> {
    @Override
    public void validate(Client entity) throws ValidatorException {


        Optional.ofNullable(entity.getId()).orElseThrow( ()-> new BaseException("Id must not be null! "));
        Optional.ofNullable(entity.getName()).orElseThrow( ()-> new BaseException("Name must not be null! "));
        Optional.ofNullable(entity.getEmail()).orElseThrow( ()-> new BaseException("Email must not be null! "));

        Optional.of(entity.getEmail())
                .filter((email) -> !email.contains("@"))
                .filter((email) -> !email.contains("."))
                .ifPresent((email) -> {throw new BaseException("The email must contain @ and . ");});
        Optional.of(entity.getName())
                .filter((e)->e.matches(""))
                .ifPresent((e)->{throw new BaseException("Name must not be empty!");});
    }
}
